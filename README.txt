CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

s-max-age-age module is for use with reverse proxies that obey s-maxage. 

s-max-age module simply sets max-age=0 and moves the cache lifetime set in Drupal to s-maxage instead.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/s_max_age

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/s_max_age?categories=All


REQUIREMENTS
------------

No special requirements.


RECOMMENDED MODULES
-------------------

No recommended modules.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules for further information.


CONFIGURATION
-------------



MAINTAINERS
-----------

Current maintainers:
 * Josh Waihi - https://www.drupal.org/u/josh-waihi
 
